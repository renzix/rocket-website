use std::fmt;
use std::borrow::Cow;

use rocket::request::FromParam;
use rocket::http::RawStr;
use rand::{self, Rng};

use log::info;

/// Table to retrieve base62 values from.
const BASE62: &[u8] = b"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

/// A _probably_ unique paste ID.
pub struct PasteID<'a>(Cow<'a, str>);

impl<'a> PasteID<'a> {
    /// Generate a _probably_ unique ID with `size` characters. For readability,
    /// the characters used are from the sets [0-9], [A-Z], [a-z]. The
    /// probability of a collision depends on the value of `size` and the number
    /// of IDs generated thus far. By default this is going to be 4 characters long
    /// which has a chance of 62^5=916,132,832 of getting overwriten
    pub fn new(size: usize) -> PasteID<'static> {
        let mut id = String::with_capacity(size);
        let mut rng = rand::thread_rng();
        for _ in 0..size {
            id.push(BASE62[rng.gen::<usize>() % 62] as char);
        }

        PasteID(Cow::Owned(id))
    }
    pub fn new_from_hash(key: &[u8]) -> PasteID<'static> {
        // This is a mur mur hash
        // i tried implementing one but it was
        // really hard so i just copied one
        let seed = 2915580697;
        let m : u64 = 0xc6a4a7935bd1e995;
        let r : u8 = 47;

        let len = key.len();
        let mut h : u64 = seed ^ ((len as u64).wrapping_mul(m));

        let endpos = len-(len&7);
        let mut i = 0;
        while i != endpos {
            let mut k : u64;

            k  = key[i+0] as u64;
            k |= (key[i+1] as u64) << 8;
            k |= (key[i+2] as u64) << 16;
            k |= (key[i+3] as u64) << 24;
            k |= (key[i+4] as u64) << 32;
            k |= (key[i+5] as u64) << 40;
            k |= (key[i+6] as u64) << 48;
            k |= (key[i+7] as u64) << 56;

            k = k.wrapping_mul(m);
            k ^= k >> r;
            k = k.wrapping_mul(m);
            h ^= k;
            h = h.wrapping_mul(m);

            // info!("h:{}, i:{}",h,i);

            i += 8;
        };

        let over = len & 7;
        if over == 7 { h ^= (key[i+6] as u64) << 48; }
        if over >= 6 { h ^= (key[i+5] as u64) << 40; }
        if over >= 5 { h ^= (key[i+4] as u64) << 32; }
        if over >= 4 { h ^= (key[i+3] as u64) << 24; }
        if over >= 3 { h ^= (key[i+2] as u64) << 16; }
        if over >= 2 { h ^= (key[i+1] as u64) << 8; }
        if over >= 1 { h ^= key[i+0] as u64; }
        if over >  0 { h = h.wrapping_mul(m); }

        h ^= h >> r;
        h = h.wrapping_mul(m);
        h ^= h >> r;
        // info!("h:{}",h);
        PasteID(Cow::Owned(h.to_string()))
    }

}

impl<'a> fmt::Display for PasteID<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

/// Returns `true` if `id` is a valid paste ID and `false` otherwise.
fn valid_id(id: &str) -> bool {
    id.chars().all(|c| {
        (c >= 'a' && c <= 'z')
            || (c >= 'A' && c <= 'Z')
            || (c >= '0' && c <= '9')
    })
}

/// Returns an instance of `PasteID` if the path segment is a valid ID.
/// Otherwise returns the invalid ID as the `Err` value.
impl<'a> FromParam<'a> for PasteID<'a> {
    type Error = &'a RawStr;

    fn from_param(param: &'a RawStr) -> Result<PasteID<'a>, &'a RawStr> {
        match valid_id(param) {
            true => Ok(PasteID(Cow::Borrowed(param))),
            false => Err(param)
        }
    }
}
