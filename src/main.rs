#![feature(proc_macro_hygiene, decl_macro)]

use rocket::{post,get,routes};
use rocket::fairing::AdHoc;
use rocket::Data;
use rocket::response::content;

mod paste_id;

use std::io;
use std::io::{Write,Read};
use std::fs::File;
use std::path::Path;

use serde_derive::Deserialize;
use rocket_contrib::json::Json;

use log::info;

use crate::paste_id::PasteID;

#[cfg(debug_assertions)]
const HOST: &str = "10.0.0.253:8000";
#[cfg(not(debug_assertions))]
const HOST: &str = "www.renzix.com";

#[get("/")]
fn index() -> &'static str {
    "Welcome to my website it is under construction so please be patient 
Right now it is just a pastebin so use <reee> to paste and <reee> to 
get that paste (please don't put anything bad things on here or else i will take it down)"
}

/// Lets you view a uploaded paste
#[get("/paste/<id>")]
fn retrieve(id: PasteID) -> Option<content::Plain<File>> {
    let filename = format!("upload/{id}", id = id);
    File::open(&filename).map(|f| content::Plain(f)).ok() //@DATABASE(renzix): Make this into diseal
}

/// Lets you view the perminant pastes
#[get("/paste/perm/<id>")]
fn retrieve_perm(id: PasteID) -> Option<content::Plain<File>> {
    let filename = format!("upload_perm/{id}", id = id);
    File::open(&filename).map(|f| content::Plain(f)).ok() //@DATABASE(renzix): Make this into diseal
}

/// Uploads a simple raw text pastebin with a random generated 3
/// char key using alphanumerical stuff
#[post("/paste", data = "<paste>", rank = 6)]
fn upload(paste: Data) -> io::Result<String> {
    let id = PasteID::new(3);
    let filename = format!("upload/{id}", id = id);
    let url = format!("{host}/paste/{id}\n", host = HOST, id = id);
    let fname = Path::new(&filename);
    paste.stream_to_file(fname)?;
    info!("Created file at {}",filename);
    Ok(url)
}

/// Makes a (nearly) perminant raw upload by using a 64bit murmur hash
#[post("/paste/perm", data = "<paste>")]
fn upload_perm(paste: Data) -> io::Result<String> {
    //@SPEED(renzix): Make this stream directly to new_from_hash so stuff can calculate while it is downloading
    // would be nice for files that take a long time to download
    let data: Vec<u8> = paste.open().bytes().map(|x| x.ok().unwrap()).collect(); // Gets data and puts it into Vec
    info!("Collected the Data");
    let id = PasteID::new_from_hash(&data);
    info!("Finished has with id: {}",id);
    let filename = format!("upload_perm/{id}", id = id);
    let url = format!("{host}/paste/perm/{id}\n", host = HOST, id = id);
    // data.stream_to_file(fname)?; //@DATABASE(renzix):Make this into diseal call
    let mut f = ::std::fs::File::create(Path::new(&filename))?;
    write!(f, "{}", data.into_iter().map(|x| x as char).collect::<String>());
    info!("Created file at {}",filename);
    Ok(url)
}

#[derive(Deserialize)]
struct JsonHeader{
    content: String,
    file_type: Option<String>,
    perm: Option<bool>,
}

impl std::fmt::Debug for JsonHeader{
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        // self.content.fmt(formatter);
        write!(fmt,"{{ content:{}, file_type:{:?}, perm:{:?} }}",self.content,self.file_type,self.perm)

    }
}

#[post("/paste", format = "application/json", data = "<paste>", rank = 1)]
fn upload_json(paste: Json<JsonHeader>) -> io::Result<String> {
    // let perm = paste.perm;
    // let url = if let Some(true) = perm {
    //     upload(paste.content)?
    // }else{
    //     upload_perm(paste.content)?
    // };
    // let data: Vec<u8> = paste.open().bytes().map(|x| x.ok().unwrap()).collect(); // Gets data and puts it into Vec
    // info!("Collected the Data");
    // let id = PasteID::new_from_hash(&data);
    // info!("Finished has with id: {}",id);
    // let filename = format!("upload_perm/{id}", id = id);
    // let url = format!("{host}/paste/perm/{id}\n", host = HOST, id = id);
    // data.stream_to_file(fname)?; //@DATABASE(renzix):Make this into diseal call
    // let mut f = ::std::fs::File::create(Path::new(&filename))?;
    // write!(f, "{}", data.into_iter().map(|x| x as char).collect::<String>());
    // info!("Created file at {}",filename);
    // Ok(url)
    println!("Json:{:?}",paste);
    Ok("reee".to_string())
}

struct Token(i64);

fn main() {
    rocket::ignite()
        .attach(AdHoc::on_attach("Token Config", |rocket| {
            println!("Adding token managed state from config...");
            let token_val = rocket.config().get_int("token").unwrap_or(-1);
            Ok(rocket.manage(Token(token_val)))
        })).mount("/",routes![index,upload, retrieve, upload_perm, retrieve_perm, upload_json]).launch();
}
